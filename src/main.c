#include <stdbool.h>
#include <stdint.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void unmap_heap(struct block_header* heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity) { .bytes = size }).bytes);
}


// Обычное успешное выделение памяти.
bool test1() {
    size_t size = 1 << 14; // 8192 * 2 == REGION_MIN_SIZE * 2
    struct block_header* heap = heap_init(size);

    if (heap == NULL) return false;
    debug_heap(stdout, heap);

    char* arr = (char *) _malloc(sizeof(char) * 256);

    if (arr == NULL) return false;
    debug_heap(stdout, heap);

    _free(arr);
    debug_heap(stdout, heap);

    unmap_heap(heap, size);

    return true;
}

// Освобождение одного блока из нескольких выделенных.
bool test2() {
    size_t size = 1 << 14; // 8192 * 2 == REGION_MIN_SIZE * 2
    struct block_header* heap = heap_init(size);

    if (heap == NULL) return false;
    debug_heap(stdout, heap);

    char* arr1 = (char *) _malloc(sizeof(char) * 256);

    if (arr1 == NULL) return false;
    debug_heap(stdout, heap);

    char* arr2 = (char *) _malloc(sizeof(char) * 256);

    if (arr2 == NULL) return false;
    debug_heap(stdout, heap);

    _free(arr1);
    debug_heap(stdout, heap);

    _free(arr2);

    unmap_heap(heap, size);

    return true;
}


// Освобождение двух блоков из нескольких выделенных.
bool test3() {
    size_t size = 1 << 14;  // 8192 * 2 == REGION_MIN_SIZE * 2
    struct block_header* heap = heap_init(size);

    if (heap == NULL) return false;
    debug_heap(stdout, heap);

    char* arr1 = (char *) _malloc(sizeof(char) * 512);

    if (arr1 == NULL) return false;
    debug_heap(stdout, heap);

    char* arr2 = (char *) _malloc(sizeof(char) * 256);

    if (arr2 == NULL) return false;
    debug_heap(stdout, heap);

    char* arr3 = (char *) _malloc(sizeof(char) * 512);

    if (arr3 == NULL) return false;
    debug_heap(stdout, heap);

    _free(arr1);
    _free(arr3);
    debug_heap(stdout, heap);
    
    _free(arr2);
    debug_heap(stdout, heap);

    unmap_heap(heap, size);

    return true;
}

// Память закончилась, новый регион памяти расширяет старый.
bool test4() {
    size_t size = 1 << 13;  // 8192 == REGION_MIN_SIZE
    struct block_header* heap = heap_init(size);

    if (heap == NULL) return false;
    debug_heap(stdout, heap);

    char* arr = (char *) _malloc(sizeof(char) * (size << 2)); // == REGION_MIN_SIZE * 4

    if (arr == NULL) return false;
    debug_heap(stdout, heap);

    _free(arr);
    debug_heap(stdout, heap);

    unmap_heap(heap, size);

    return true;
}

// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
bool test5() {
    size_t size = 1 << 13;  // 8192 == REGION_MIN_SIZE
    struct block_header* heap = heap_init(size);

    if (heap == NULL) return false;
    debug_heap(stdout, heap);

    char* arr1 = (char *) _malloc(sizeof(char) * (size >> 1)); // == REGION_MIN_SIZE / 2

    if (arr1 == NULL) return false;
    debug_heap(stdout, heap);


    void* after_region = heap->contents + (heap->capacity).bytes;
    void* middle = mmap(after_region, sizeof(char), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);


    char* arr2 = (char *) _malloc(sizeof(char) * size); // == REGION_MIN_SIZE

    if (arr2 == NULL) return false;
    debug_heap(stdout, heap);

    _free(arr1);
    _free(arr2);
    debug_heap(stdout, heap);

    unmap_heap(middle, sizeof(char));
    unmap_heap(heap, size);

    return true;
}

int main() {
    bool (*tests[])(void) = { test1, test2, test3, test4, test5 };

    for (int i = 0; i < sizeof(tests) / sizeof(tests[0]); i++) {
        if (tests[i]() == true) {
            fprintf(stdout, "Test %d passed", i);
        } else {
            fprintf(stdout, "Test %d failed", i);
        }
    }

    return 0;
}
